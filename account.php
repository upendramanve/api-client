<?php

$error = '';
$success='';

 //   echo  md5(uniqid());

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<title>
    Api Client app
</title>
<body >
  <!-- Fixed navbar -->
  <?php include('header.php');?>
       

        <div class="container">

            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
                    </p>
                    <div class="jumbotron">
                        <h1 id="welcome-message-tab"></h1>
                        <p id="welcome-sub_message-tab"></p>
                    </div>
                    <div class="row" id="user-ussage">
                        <div class="col-xs-6 col-lg-3"><div class="panel panel-default"><div class="panel-heading"><h4>Search</h4></div><div class="panel-body">6</div></div></div>
                        
                    </div><!--/row-->
                </div><!--/.col-xs-12.col-sm-9-->

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <?php include('leftmenu.php');?>
                </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->

            <hr/>
                <?php include('footer.php');?>
           

        </div><!--/.container-->

</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
            var x = readCookie('token');
   // $.ajax({
   //     method: "GET",
   //     url: "http://localhost:8080/api-sample/checkLogin/"+x,
   //     beforeSend: function(xhr) {
   //         xhr.setRequestHeader("Authorization", x);
    //      },
   //     dataType: 'json'
   //   })
   //     .done(function( msg ) {
     //       if(msg.status=='success')
     //       {
      //          getUserInfo();
      //      }
       //     else
        //    {
        //        eraseCookie('token');
         //       window.location.href = "http://localhost:8080/api-client/login.php";
         //   }
         // alert( "Data Saved: " + msg );
       // });
        getUserInfo();
    });
    
    function getUserInfo()
    {
        var x = readCookie('token');
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/getUserInfo",
             beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", x);
          },
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    $('#welcome-message-tab').html('Welcome '+capitalizeFirstLetter(msg.response.first_name)+' '+capitalizeFirstLetter(msg.response.last_name));
                    $('#welcome-sub_message-tab').html('Email address: '+msg.response.email+' | Last login: '+unixEpochTime_TO_Date_DDMMYY(msg.response.last_login, " Local"));
                    getUserUsage(msg.response.user_id);
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    function getUserUsage(user_id)
    {
        var x = readCookie('token');
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/users/ussage/"+user_id,
             beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", x);
          },
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    var disp_string ='';
                    var  results = msg.response.results;
                    $.each( results, function( key, value ) {
                    disp_string += '<div class="col-xs-6 col-lg-3"><div class="panel panel-default"><div class="panel-heading"><h4>'+capitalizeFirstLetter(value.action_type.replace(/_/g, ' '))+'</h4></div><div class="panel-body">'+value.action_count+'</div></div></div>';
                });
                $('#user-ussage').html(disp_string);
                }
               
             // alert( "Data Saved: " + msg );
            });
    }
    
   
    
    </script>
