<?php
$error = '';
$success='';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<title>
    Api Client app
</title>
<body >
  <!-- Fixed navbar -->
  <?php include('header.php');?>
       

        <div class="container">

            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-12 col-sm-9">
                 <?php if($error!=''){ ?>
                    <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
                   <?php } ?>
                    <?php if($success!=''){ ?>
                    <div class="alert alert-success" role="alert"><?php echo $success; ?></div>
                   <?php } ?>  

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 >Login</h3>
                  </div>
                  <div class="panel-body">
                   <form class="form-horizontal" method="post">
                    <div class="form-group">
                      <label for="" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email-feild" name="email" placeholder="Email" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="password-feild" name="password" placeholder="Password" />
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="submit-button" name="submit" value="submit" class="btn btn-default">Sign in</button>
                      </div>
                    </div>
                  </form>
                  </div>
                </div>
                </div><!--/.col-xs-12.col-sm-9-->

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <?php include('leftmenu.php');?>
                </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->

            <hr/>
                <?php include('footer.php');?>
           

        </div><!--/.container-->

</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        //login check
     //    var x = readCookie('token');
   //      $.ajax({
  //      method: "GET",
    //    url: "http://localhost:8080/api-sample/checkLogin/"+x,
    //    dataType: 'json'
    //  })
      //  .done(function( msg ) {
      //      if(msg.status=='success')
        //    {
      //          window.location.href = "http://localhost:8080/api-client/account.php";
       //     }
      //      else
        //    {
                
        //    }
         // alert( "Data Saved: " + msg );
        //});
        
        
        $('body').on('click','.submit-button',function(){
            
        
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/api-sample/login",
        data: { email: $('#email-feild').val(), password: $('#password-feild').val() },
        dataType: 'json'
      })
        .done(function( msg ) {
            if(msg.status == 'success')
        {
            document.cookie="token="+msg.response.user_token;
            window.location.href = "http://localhost:8080/api-client/account.php";
        }
        else
        {
            alert(  msg.message );
        }   
        });
  });
    });
    
    </script>
