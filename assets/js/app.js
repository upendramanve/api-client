// Cookies
        function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";

            var fixedName = '<%= Request["formName"] %>';
            name = fixedName + name;

            document.cookie = name + "=" + value + expires + "; path=/";
        }
function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function eraseCookie(name) {
            createCookie(name, "", -1);
        }
        
function unixEpochTime_TO_Date_DDMMYY (unixEpochTime, returnUTC) {
    var year, month, day;
    var dateObj = new Date (unixEpochTime * 1000);

    if (returnUTC) {
        year    = dateObj.getUTCFullYear ();
        month   = dateObj.getUTCMonth ();
        day     = dateObj.getUTCDate ();
    }
    else {
        year    = dateObj.getFullYear ();
        month   = dateObj.getMonth ();
        day     = dateObj.getDate ();
    }

    //-- Month starts with 0, not 1.  Compensate.
    month      += 1;

    /*-- Since we want DDMMYY, we need to trim the year and zero-pad
        the day and month.
        Note:  Use YYMMDD, for sorting that makes sense.
    */
    year    = (""  + year) .slice (-2);
    month   = ("0" + month).slice (-2);
    day     = ("0" + day)  .slice (-2);

    return day+'-' + month+ '-' + year;
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function paginationLink(total,offset,limit) {
    var generated_links = '';
    if(limit>0){
        var total_links = Math.ceil(total/limit);
        var current_link = (offset/limit)+1;
        for (var i = 1; i <= total_links; i++) {
            if(current_link==i)
            {
              generated_links += '<li class="active"><a>'+i+'</a></li>'  
            }
            else
            {
            generated_links += '<li><a data-offset='+((i-1)*limit)+' class="paginate" href="javascript:;">'+i+'</a></li>';
            }
        }
    }
   return generated_links;
}

    $(document).ready(function(){      
        
         $('body').on('click','#logout-control',function(){
          var x = readCookie('token');  
        
    
          //alert( "Data Saved: " + msg );
          
        eraseCookie('token');
       // window.location.href = "http://localhost:8080/api-client/login.php";
       
  });
        
           });
    
  