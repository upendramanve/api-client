<?php

$error = '';
$success='';

   

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<title>
    Api Client app
</title>
<body >
  <!-- Fixed navbar -->
  <?php include('header.php');?>
       

        <div class="container">

            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
                    </p>
                    <div class="jumbotron">
                        <h1 id="welcome-message-tab"></h1>
                        <p id="welcome-sub_message-tab"></p>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                    </div><!--/row-->
                </div><!--/.col-xs-12.col-sm-9-->

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <?php include('leftmenu.php');?>
                </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->

            <hr/>
                <?php include('footer.php');?>
           

        </div><!--/.container-->

</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
            var x = readCookie('token');
    //$.ajax({
     //   method: "GET",
      //  url: "http://localhost:8080/api-sample/checkLogin/"+x,
     //   dataType: 'json'
     // })
     ///   .done(function( msg ) {
        //    if(msg.status=='success')
         //   {
         //       getUser();
                //searchUsers();
         //   }
        //    else
        //    {
         //       eraseCookie('token');
        //        window.location.href = "http://localhost:8080/api-client/login.php";
        //    }
         // alert( "Data Saved: " + msg );
        //});
        getUser();
    });
    
    function getUser()
    {
        var x = readCookie('token');
        var userid = <?php echo isset($_GET['userid'])?(int)$_GET['userid']:0;  ?>;
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/users/"+userid,
            beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", x);
          },
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    $('#welcome-message-tab').html('Welcome '+msg.response.first_name+' '+msg.response.last_name);
                    $('#welcome-sub_message-tab').html('Email address: '+msg.response.email+' | Last login: '+unixEpochTime_TO_Date_DDMMYY(msg.response.last_login, " Local"));
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    function searchUsers()
    {
        var x = readCookie('token');
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/searchUser/"+x,
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    console.log(msg.response);
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    </script>
