 <nav id="header" class="navbar navbar-fixed-top">
            <div id="header-container" class="container navbar-container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="brand" class="navbar-brand" href="index.php"> Api Client app</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <?php if(isset($_COOKIE["token"]) && $_COOKIE["token"]!='' ){ ?>
                        <li><a href="account.php">Account </a></li>
                        <li><a href="find.php">Find people </a></li>
                        <?php /*<li><a id="logout-control" href="javascript:;">Logout</a></li> */ ?>
                        <li><a  href="logout.php">Logout</a></li>
                        <?php }else{ ?>
                        <li><a href="login.php">Login</a></li>
                        <li><a href="signup.php">Signup </a></li>
                        <?php } ?>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div><!-- /.container -->
        </nav><!-- /.navbar -->