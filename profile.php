<?php

$error = '';
$success='';

   

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<title>
    Api Client app
</title>
<body >
  <!-- Fixed navbar -->
  <?php include('header.php');?>
       

        <div class="container">

            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
                    </p>
                    <div class="jumbotron">
                        <h1 id="welcome-message-tab"></h1>
                        <p id="welcome-sub_message-tab"></p>
                    </div>
                      <div class="row">
                        <div class="col-md-8">
                          <form class="form-horizontal" method="post" id="profile-settings"  enctype="multipart/form-data"  >
                            <fieldset>
                                <input type="hidden" name="_METHOD" value="PUT"/>
                                <input type="hidden" name="user_id" id="user_id_control" value=""/>
                              <!-- Form Name -->
                              <legend>Profile settings</legend>
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" id="" for="firstname">First name</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="First name" id="first_name_control" class="form-control" name="first_name" value="" />
                                </div>
                              </div>
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" id="" for="firstname">Last name</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Last name" id="last_name_control" class="form-control" name="last_name" value="" />
                                </div>
                              </div>
                              
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" id="" for="firstname">Gender</label>
                                <div class="col-sm-9">
                                    <label class="checkbox-inline">
                                        <input type="radio" id="gender_male_control" name="gender" value="male" /> Male
                                      </label>
                                      <label class="checkbox-inline">
                                        <input type="radio" id="gender_female_control" name="gender" value="female" /> Female
                                      </label>
                                </div>
                              </div>
                              
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" id="" for="email">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Email" id="email_control" class="form-control" name="email" disabled value="" />
                                </div>
                              </div>
                              
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="textinput">Address </label>
                                <div class="col-sm-9">
                                    <textarea placeholder="Address" name="address" id="address_control" class="form-control"></textarea>
                                </div>
                              </div>

                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="textinput">City</label>
                                <div class="col-sm-9">
                                  <input type="text" placeholder="City" name="city" value="" id="city_control" class="form-control" />
                                </div>
                              </div>

                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="textinput">State</label>
                                <div class="col-sm-3">
                                    <input type="text" placeholder="State" name="state" value="" id="state_control" class="form-control" />
                                </div>

                                <label class="col-sm-3 control-label" for="textinput">Postcode</label>
                                <div class="col-sm-3    ">
                                  <input type="text" placeholder="Post Code" name="postcode" value="" id="postcode_control" class="form-control" />
                                </div>
                              </div>



                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="textinput">Country</label>
                                <div class="col-sm-9">
                                  <input type="text" placeholder="Country" name="country" value="" id="country_control" class="form-control" />
                                </div>
                              </div>
                              <!-- Text input-->
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="textinput">CV</label>
                                <div class="col-sm-9">
                                  <input type="file"  name="cv" value="" id="cv_control"   />
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="textinput"></label>
                                <div class="col-sm-9" id="uploaded-cv">
                                  
                                </div>
                              </div>
                              

                              <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                  </div>
                                </div>
                              </div>

                            </fieldset>
                          </form>
                         
                            <form class="form-horizontal" method="post" id="add-interests"  >
                            <fieldset>
                              <!-- Form Name -->
                              <legend>Interests</legend>
                              
                              <div class="row" id='all-interests'>
                                 
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                            </form>    
                            
                            
                            
                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->
                </div><!--/.col-xs-12.col-sm-9-->

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <?php include('leftmenu.php');?>
                </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->

            <hr/>
                <?php include('footer.php');?>
           

        </div><!--/.container-->

</body>
</html>
<script type="text/javascript">
    var user_id = 0;
    $(document).ready(function(){
            var x = readCookie('token');
            
  //  $.ajax({
   //     method: "GET",
   //     url: "http://localhost:8080/api-sample/checkLogin/"+x,
   //     dataType: 'json'
   //   })
      //  .done(function( msg ) {
      //      if(msg.status=='success')
       //     {
      //         getUserInfo();
                
                //searchUsers();
      //      }
       //     else
          //  {
          //      eraseCookie('token');
         //       window.location.href = "http://localhost:8080/api-client/login.php";
          //  }
         // alert( "Data Saved: " + msg );
      //  });
        getUserInfo();
        /********START TO VALIDATE THE FORM**********/
		
		$("#profile-settings").validate({
			
			rules:{
				first_name	:	{	required:true},
                                last_name	:	{	required:true},
				gender          :	{	required:true}
			},
			
			messages:{
				first_name	:	{ required: "Please enter first name" },
				last_name	:	{ required:"Please enter last name" },
                                gender          :       { required:"Please choose a gender" }

			},
                        submitHandler: function(form) {
                            //alert(user_id);
                            //return false;
                            // do other things for a valid form
                            //form.submit();
                            //console.log($('#profile-settings').serialize());
                            var x = readCookie('token');
                           var  userid = $('#user_id_control').val();
                            $.ajax({
                                method: "PUT",
                                url: "http://localhost:8080/api-sample/users/"+userid,
                                data: $('#profile-settings').serialize(),
                                 beforeSend: function(xhr) {
                                    xhr.setRequestHeader("Authorization", x);
                                    xhr.setRequestHeader("Content-Type", 'multipart/form-data');
                                  },
                                dataType: 'json'
                              })
                                .done(function( msg ) {
                                    if(msg.status == 'success')
                                {
                                    
                                    var data = new FormData();
                                jQuery.each(jQuery('#cv_control')[0].files, function(i, file) {
                                    data.append('file-'+i, file);
                                });
                                jQuery.ajax({
                                    url:  "http://localhost:8080/api-sample/users/cv/"+userid,
                                    data: data,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    beforeSend: function(xhr) {
                                        xhr.setRequestHeader("Authorization", x);
                                      },
                                    type: 'POST',
                                    success: function(data){
                                        alert(data.message);
                                        getUserInfo();
                                    }
                                });
                                    //window.location.href = "http://localhost:8080/api-client/profile.php";
                                }
                                else
                                {
                                    alert(  msg.message );
                                }   
                                });
                                
                                
                                
                            
                            return false;
                            
                            
                          },
			errorClass: "form-error",
			errorPlacement: function(error, element) {
				
					error.appendTo(element.parent());
					//error.prependTo(element.parent());
				},
			invalidHandler: function(form, validation){
					$('div.form-error').remove();
				}
		});
                
                $("#add-interests").validate({
			
			rules:{
				'userInterests[]'          :	{	required:true}
			},
			
			messages:{
                                'userInterests[]'          :       { required:"Please choose atleast one interest" }

			},
                        submitHandler: function(form) {
                            //alert(user_id);
                            //return false;
                            // do other things for a valid form
                            //form.submit();
                            console.log($('#add-interests').serialize());
                           var  userid = $('#user_id_control').val();
                           var x = readCookie('token');
                            $.ajax({
                                method: "POST",
                                url: "http://localhost:8080/api-sample/users/"+userid+"/interests",
                                data: $('#add-interests').serialize(),
                                 beforeSend: function(xhr) {
                                    xhr.setRequestHeader("Authorization", x);
                                  },
                                dataType: 'json'
                              })
                                .done(function( msg ) {
                                    if(msg.status == 'success')
                                {
                                   // window.location.href = "http://localhost:8080/api-client/profile.php";
                                   //alert(  msg.message );
                                   getUserInfo();
                                }
                                else
                                {
                                   //s alert(  msg.message );
                                }   
                                });
                            
                            return false;
                            
                            
                          },
			errorClass: "form-error",
			errorPlacement: function(error, element) {
				
					error.appendTo(element.parent());
					//error.prependTo(element.parent());
				},
			invalidHandler: function(form, validation){
					$('div.form-error').remove();
				}
		});
                
		/********        END FORM VALIDATION         *******/
 
    });
    
    function getUserInfo()
    {
        var x = readCookie('token');
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/getUserInfo",
            beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", x);
          },
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    $('#welcome-message-tab').html('Welcome '+capitalizeFirstLetter(msg.response.first_name)+' '+capitalizeFirstLetter(msg.response.last_name));
                    $('#welcome-sub_message-tab').html('Email address: '+msg.response.email+' | Last login: '+unixEpochTime_TO_Date_DDMMYY(msg.response.last_login, " Local"));
                    $('#first_name_control').val(msg.response.first_name);
                    $('#last_name_control').val(msg.response.last_name);
                    $('#email_control').val(msg.response.email);
                    $('#address_control').html(msg.response.address);
                    $('#city_control').val(msg.response.city);
                    $('#state_control').val(msg.response.state);
                    $('#postcode_control').val(msg.response.postcode);
                    $('#country_control').val(msg.response.country);
                    if(msg.response.gender=='male')
                    {
                        $('#gender_male_control').attr('checked','checked');
                    }
                    else if(msg.response.gender=='female')
                    {
                        $('#gender_female_control').attr('checked','checked');
                    }
                    $('#user_id_control').val(msg.response.user_id);
                    if(msg.response.cv_file != '')
                    {
                        $('#uploaded-cv').html('<a href="'+"http://localhost:8080/api-sample/file/"+msg.response.cv_file+'">Uploaded Cv file</a>');
                    }
                    getInterests();
                 getUserInterests(msg.response.user_id);
                        
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    function searchUsers()
    {
        var x = readCookie('token');
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/searchUser/"+x,
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    console.log(msg.response);
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    function getInterests()
    {
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/interests?all=yes",
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                   // console.log(msg.response);
                    var  results = msg.response.results;
                    var disp_res_string = '';
                     $.each( results, function( key, value ) {
                         disp_res_string += '<div class="col-md-4"><div class="checkbox"><label><input type="checkbox" name="userInterests[]" value="'+value.intid+'" class="user-interests-checkbox" /> '+value.intname+' </label></div></div>';
                     });
                     $('#all-interests').html(disp_res_string);
                }
                else
                {
                    console.log(msg.response);
                }
             // alert( "Data Saved: " + msg );
            });
    }
    function getUserInterests(userid)
    {
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/users/"+userid+"/interests?all=yes",
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    //console.log(msg.response);
                   // alert($('.user-interests-checkbox').length);
                   var  results = msg.response.results;
                   $.each(results, function( key, value ) {
                        $(".user-interests-checkbox").each(function() {
                            //console.log($(this).val());
                            if($(this).val()==value.intid)
                            {
                               $(this).attr('checked','checked');  
                            }
                        });
                    });
                    
                }
                else
                {
                    console.log(msg.response);
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    </script>
