<?php

$error = '';
$success='';

   

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<title>
    Api Client app
</title>
<body >
  <!-- Fixed navbar -->
  <?php include('header.php');?>
       

        <div class="container">

            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
                    </p>
                    <div class="jumbotron">
                        <form class="form-inline">
                            <div class="form-group">
                              <label for="">Keyword</label>
                              <input type="text" class="form-control" id="keyword-search-text" placeholder="Enter Keyword to search " />
                            </div>
                            <select class="form-control orderby-search-control">
                                <option value="">Sort by</option>
                                <option value="added_on">Joined date</option>
                                <option value="first_name">First name</option>
                                <option value="last_name">Last name</option>
                                <option value="email">Email</option>
                              </select>
                            <select class="form-control order-search-control">
                                <option value="desc">Descending</option>
                                <option value="asc">Ascending</option>
                                
                              </select>
                            <button type="button" class="btn btn-default search-results">Search</button>
                          </form>
                    </div>
                    <ol class="breadcrumb">
                        Showing <strong id="result-offset">0</strong> to <strong id="result-limit">6</strong> of <strong id="result-total">18</strong> Results
                      </ol>
                    <div class="row" id="search-results">
                        <div class="col-xs-6 col-lg-4">
                            <h2>Heading</h2>
                            <p></p>
                            <p><a class="btn btn-default" href="#" role="button">View details �</a></p>
                        </div><!--/.col-xs-6.col-lg-4-->
                        
                    </div><!--/row-->
                    <nav>
                        <ul class="pagination">
                          
                        </ul>
                      </nav>
                </div><!--/.col-xs-12.col-sm-9-->

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <div class="list-group">
                        <?php include('leftmenu.php');?>
                    </div>
                </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->

            <hr/>
                <?php include('footer.php');?>
           

        </div><!--/.container-->

</body>
</html>
<script type="text/javascript">
     var offset = 0;
     var orderby = '';
     var order = 'desc';
    $(document).ready(function(){
       
            var x = readCookie('token');
   // $.ajax({
    //    method: "GET",
    //    url: "http://localhost:8080/api-sample/checkLogin/"+x,
    //    dataType: 'json'
    //  })
     //   .done(function( msg ) {
        //    if(msg.status=='success')
        //    {
        //        getUserInfo();
       //         searchUsers();
        //    }
        //    else
        //    {
        //        eraseCookie('token');
      //          window.location.href = "http://localhost:8080/api-client/login.php";
        //    }
         // alert( "Data Saved: " + msg );
      //  });
        //getUserInfo();
        searchUsers();
        $('body').on('click','.search-results',function(){
            offset = 0;
            orderby = $('.orderby-search-control').val();
            order = $('.order-search-control').val();
            searchUsers();
        });
        
        $('body').on('click','.paginate',function(){
            orderby = $('.orderby-search-control').val();
            order = $('.order-search-control').val();
          offset = $(this).attr('data-offset');
            searchUsers();
        });
        
        $('body').on('change','.orderby-search-control',function(){
            orderby = $('.orderby-search-control').val();
            order = $('.order-search-control').val();
            offset = 0;
            searchUsers();
        });
        $('body').on('change','.order-search-control',function(){
            orderby = $('.orderby-search-control').val();
            order = $('.order-search-control').val();
            offset = 0;
            searchUsers();
        });
        
    });
    
    function getUserInfo()
    {
        var x = readCookie('token');
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/getUserInfo",
            beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", x);
          },
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    $('#welcome-message-tab').html('Welcome '+msg.response.first_name+' '+msg.response.last_name);
                    $('#welcome-sub_message-tab').html('Email address: '+msg.response.email+' | Last login: '+unixEpochTime_TO_Date_DDMMYY(msg.response.last_login, " Local"));
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    function searchUsers()
    {
        var x = readCookie('token');
        var keyword = $.trim($('#keyword-search-text').val());
        $.ajax({
            method: "GET",
            url: "http://localhost:8080/api-sample/users"+"?keyword="+keyword+"&offset="+offset+"&orderby="+orderby+"&order="+order,
            beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", x);
          },
            dataType: 'json'
          })
            .done(function( msg ) {
                if(msg.status=='success')
                {
                    
                    var  results = msg.response.results;
                    var disp_res_string = '';
                    $.each( results, function( key, value ) {
                       // $.each( value, function( key, value ) {
                        //    console.log( key + ": " + value );
                        //});
                       // console.log(value.first_name);
                       disp_res_string += '<div class="col-xs-6 col-lg-4"><h2>'+capitalizeFirstLetter(value.first_name)+' '+capitalizeFirstLetter(value.last_name)+'</h2><p> Email: '+value.email+' </p><p><a class="btn btn-default" href="user.php?userid='+value.user_id+'">View details </a></p></div>';
                      });
                   //s console.log(results);
                   $('#search-results').html(disp_res_string);
                   $('#result-offset').html(msg.response.offset+1);
                   var displimit = msg.response.limit+msg.response.offset;
                    if(displimit > msg.response.total)
                    {
                        displimit = msg.response.total;
                    }
                   $('#result-limit').html(displimit);
                   $('#result-total').html(msg.response.total);
                   $('.pagination').html(paginationLink(msg.response.total,msg.response.offset,msg.response.limit));
                }
                else
                {
                    eraseCookie('token');
                    window.location.href = "http://localhost:8080/api-client/login.php";
                }
             // alert( "Data Saved: " + msg );
            });
    }
    
    
    
    </script>
