<?php

setcookie("token", "", time()-3600);
redirect('http://localhost:8080/api-client/login.php');

function redirect($url) {
    ob_start();
    header('Location: '.$url);
    ob_end_flush();
    die();
}
?>