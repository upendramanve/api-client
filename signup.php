<?php
session_start();
$error = '';
$success='';
$_SESSION['abc'] = 'sadsewae';
if(isset($_POST['submit']) && $_POST['submit']!='' )
{
    $first_name = urlencode(trim($_POST['first_name']));
    $last_name = urlencode(trim($_POST['last_name']));
    $email = urlencode(trim($_POST['email']));
    $password = urlencode(trim($_POST['password']));
    
    $ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://localhost:8080/api-sample/users");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "first_name=$first_name&last_name=$last_name&email=$email&password=$password");

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = json_decode(curl_exec ($ch));

curl_close ($ch);


if(isset($server_output->status)){    
//print('<pre>');
//print_r($server_output);
//print('</pre>');
//echo $server_output->status;
    if($server_output->status =='fail')
    {
        //handle faluire
        $error = $server_output->message;
    }
    elseif($server_output->status=='success')
    {
        //handle success
        $success = $server_output->message;
    }
    else
    {
        //handle else case
    }
    
}

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<title>
    Api Client app
</title>
<body >
  <!-- Fixed navbar -->
  <?php include('header.php');?>
       

        <div class="container">

            <div class="row row-offcanvas row-offcanvas-right">

                <div class="col-xs-12 col-sm-9">
                   <?php if($error!=''){ ?>
                    <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
                   <?php } ?>
                    <?php if($success!=''){ ?>
                    <div class="alert alert-success" role="alert"><?php echo $success; ?></div>
                   <?php } ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 >Signup</h3>
                  </div>
                  <div class="panel-body">
                   <form class="form-horizontal" method="post">
                       <div class="form-group">
                      <label for="" class="col-sm-2 control-label">First name</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="" name="first_name" placeholder="First name" />
                      </div>
                    </div>
                     <div class="form-group">
                      <label for="" class="col-sm-2 control-label">Last name</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="" name="last_name" placeholder="Last name" />
                      </div>
                    </div>  
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="Email" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input type="password" class="form-control" id="" name="password" placeholder="Password" />
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" name="submit" value="submit">Sign up</button>
                      </div>
                    </div>
                  </form>
                  </div>
                </div>
                </div><!--/.col-xs-12.col-sm-9-->

                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <?php include('leftmenu.php');?>
                </div><!--/.sidebar-offcanvas-->
            </div><!--/row-->

            <hr/>
                <?php include('footer.php');?>
           

        </div><!--/.container-->

</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        //login check
         var x = readCookie('token');
         $.ajax({
        method: "GET",
        url: "http://localhost:8080/api-sample/checkLogin/"+x,
        dataType: 'json'
      })
        .done(function( msg ) {
            if(msg.status=='success')
            {
                window.location.href = "http://localhost:8080/api-client/account.php";
            }
            else
            {
                
            }
         // alert( "Data Saved: " + msg );
        });
        
        
       
    });
    
    </script>
